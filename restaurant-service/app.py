from flask import Flask,jsonify, request
from flask_rest_controller import Controller, set_routing
import mysql.connector
import json

app = Flask(__name__)

#Return: lista di tutti i ristoranti
@app.route('/restaurants', methods = ["GET"])
def getRistoranti():
  mydb = mysql.connector.connect(
   host="mysql",
   user="root",
   passwd="ruttino",
   database="cloudcomputing"
  )               
  mycursor = mydb.cursor()
  mycursor.execute("SELECT * FROM ristoranti")
  myresult = mycursor.fetchall()
  payload = []
  row_headers=[x[0] for x in mycursor.description]
  for result in myresult:
    payload.append(dict(zip(row_headers,result)))
  return json.dumps(payload)

#Return: info di un singolo ristorante
#Input : INT id ristorante
@app.route('/restaurant',methods=["GET"])
def getRistorante():
  restaurantId = request.args.get('id')
  mydb = mysql.connector.connect(
   host="mysql",
   user="root",
   passwd="ruttino",
   database="cloudcomputing"
  )               
  mycursor = mydb.cursor()
  sql = "SELECT * FROM ristoranti WHERE id = %s"
  idr = (restaurantId,)
  mycursor.execute(sql, idr)
  myresult = mycursor.fetchall()
  payload = []
  row_headers=[x[0] for x in mycursor.description]
  for result in myresult:
    payload.append(dict(zip(row_headers,result)))
  return json.dumps(payload) 

#Endpoint per liveness
@app.route("/healtz")
def healtz():
 return Response(status=200, mimetype='application/json') 

if __name__ == "__main__":
    app.run(debug=True,port=8082,host='0.0.0.0')
