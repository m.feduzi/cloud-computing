from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from email.mime.text import MIMEText
#import base64
from base64 import urlsafe_b64encode
from googleapiclient import errors
#from flask import Flask, jsonify
#from flask_rest_controller import Controller, set_routing
import requests
from flask import Response
from flask import request
from pprint import pprint
import json

SCOPES = ['https://www.googleapis.com/auth/gmail.send']
def handle(req):
    req_parsed = json.loads(req)
    sendMail(req_parsed)
    return req 

def sendMail(richiesta):
    to = richiesta["to"]
    soggetto = richiesta["soggetto"]
    messaggio = richiesta["messaggio"]
    #to = "zucca86@hotmail.it"
    #soggetto = "soggetto"
    #messaggio = "messsaggino"
    creds = None
    if os.path.exists('/home/app/function/token.pickle'):
        with open('/home/app/function/token.pickle', 'rb') as token:
            creds = pickle.load(token, encoding="latin1")
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                '/home/app/function/credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        with open('/home/app/function/token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    
    service = build('gmail', 'v1', credentials=creds)
    message = create_message("feduzi.sag@gmail.com",to,soggetto,messaggio)
    send_message(service,"me",message)
    return Response("{'messaggio':'ok'}", status=200, mimetype='application/json')        

def create_message(sender, to, subject, message_text):
  message = MIMEText(message_text)
  message['to'] = to
  message['from'] = sender
  message['subject'] = subject
  encoded_message = urlsafe_b64encode(message.as_bytes())
  return {'raw': encoded_message.decode()}

def send_message(service, user_id, message):
  try:
    message = (service.users()
     .messages()
     .send(userId=user_id, body=message) 
     .execute())
    print('Message Id: %s' % message['id'])
    return message
  except errors.HttpError:
    print('An error occurred:')
