from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from email.mime.text import MIMEText
import base64
from googleapiclient import errors
from flask import Flask, jsonify
#from flask_rest_controller import Controller, set_routing
import requests
from flask import Response
from flask import request
from base64 import urlsafe_b64encode

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.send']

app = Flask(__name__)

@app.route("/", methods=["POST"])
def sendMail():
    richiesta = request.form
    to = richiesta["to"]
    soggetto = richiesta["soggetto"]
    messaggio = richiesta["messaggio"]
    creds = None
    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token,encoding="latin1")
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json', SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)
    
    service = build('gmail', 'v1', credentials=creds)
    #message = create_message("feduzi.sag@gmail.com","zucca86@hotmail.it","rerwe","dede")
    message = create_message("feduzi.sag@gmail.com",to,soggetto,messaggio)
    send_message(service,"me",message)
    return Response("{'messaggio':'ok'}", status=200, mimetype='application/json')        

def create_message(sender, to, subject, message_text):
  message = MIMEText(message_text)
  message['to'] = to
  message['from'] = sender
  message['subject'] = subject
  encoded_message = urlsafe_b64encode(message.as_bytes())
  return {'raw': encoded_message.decode()}
 
def send_message(service, user_id, message):
  try:
    message = (service.users().messages().send(userId=user_id, body=message)
               .execute())
    print('Message Id: %s' % message['id'])
    return message
  except errors.HttpError:
    print('An error occurred: ')

@app.route("/healtz")
def healtz():
 return Response(status=200, mimetype='application/json') 
 

if __name__ == "__main__":
    app.run(debug=True,port=8081,host='0.0.0.0')

