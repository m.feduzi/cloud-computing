import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import AllRestaurants from '@/components/AllRestaurants'
import Restaurant from '@/components/Restaurant'

Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children: [
        {
          path: '',
          name: 'AllRestaurants',
          component: AllRestaurants
        },
        {
          path: ':restaurantId',
          name: 'Restaurant',
          component: Restaurant
        }
      ]
    }
  ]
})
