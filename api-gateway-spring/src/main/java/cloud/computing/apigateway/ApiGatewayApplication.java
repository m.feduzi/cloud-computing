package cloud.computing.apigateway;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.hystrix.HystrixCircuitBreakerFactory;
import org.springframework.cloud.netflix.hystrix.ReactiveHystrixCircuitBreakerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixObservableCommand;

@RestController
@EnableCircuitBreaker
@SpringBootApplication
@RequestMapping("/fallback")
public class ApiGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayApplication.class, args);
	}
	//restaurant-service order-service
	@Bean
	public RouteLocator myRoutes(RouteLocatorBuilder builder) {
	    return builder.routes()
	        .route(p -> p
	            .path("/restaurants")
	            .filters(f -> f.addResponseHeader("Access-Control-Allow-Origin","*")
	            		        .hystrix(c -> c.setName("dde").setFallbackUri("forward:/fallback/first")))
	            .uri("http://restaurant-service/restaurants"))
	        .route(p -> p
	        	.path("/restaurant")
	        	.filters(f -> f.addResponseHeader("Access-Control-Allow-Origin","*")
	        					.hystrix(c -> c.setName("dde").setFallbackUri("forward:/fallback/first")))
	        	.uri("http://restaurant-service/restaurant"))
	        .route(p -> p
	        	.path("/orders")
	        	.filters(f -> f.addResponseHeader("Access-Control-Allow-Origin","*")
	        					.hystrix(c -> c.setName("dde").setFallbackUri("forward:/fallback/first")))
	        	.uri("http://order-service/orders"))
	        .build();
	}
	
	
    @GetMapping("/first")
    public String firstServiceFallback(){
        return "Servizio momentaneamente non disponibile";
    }
}
