from flask import Flask, jsonify, request
from flask_rest_controller import Controller, set_routing
import requests
import json
from flask import Response
import mysql.connector
'''per debug
import sys
import logging
logging.basicConfig(level=logging.DEBUG)
sys.stdout.flush()'''

app = Flask(__name__)

#Return: prenotazione effettuata o rifiutata
#Input: INT id ristorante, INT numero posti prenotati, 
#       DATA di prenotazione, STRING email, STRING nome
@app.route('/orders', methods=["POST"])
def order():
  data = request.form
  restorauntId = data["idr"]
  seatsNumber = int(data["posti"])
  prenotation = data["data"]
  to = data["to"]
  name = data["nome"]
  soggetto = "Prenotazione ristorante"
  messaggio = "Gentile "+ name + ", la sua prenotazione NON è andata a buon fine perchè non ci sono abbastanza posti disponibili per il giorno "+ prenotation + "."
  mydb = mysql.connector.connect(
    host="mysql",
    user="root",
    passwd="ruttino",
    database="prenotazioni"
  )               
  mycursor = mydb.cursor() 
  restaurantsParams = {"id": restorauntId}
  ristorante = requests.get("http://restaurant-service/restaurant", params=restaurantsParams).content
  parsed = json.loads(ristorante)
  totalSeats = parsed[0]["posti"]

  sql ="SELECT sum(posti) as posti_prenotati FROM prenotazioni WHERE id_ristorante = %s AND data_prenotazione = %s"
  idr = (restorauntId,prenotation)
  mycursor.execute(sql, idr)
  myresult = mycursor.fetchall()
  if myresult[0][0] is not None:
   currentSeats = int(myresult[0][0])
  else:
   currentSeats = 0

  if currentSeats + seatsNumber <= totalSeats:
    sql = "INSERT INTO prenotazioni (id_ristorante, posti,data_prenotazione) VALUES (%s, %s, %s)"
    val = (restorauntId, seatsNumber,prenotation)
    mycursor.execute(sql, val)
    mydb.commit()
    #messaggio = "Gentile " + name + ", la sua prenotazione per il giorno " + prenotation  + " per "+ str(seatsNumber) +" PERSONE è stata effettuata con successo."
    messaggio = "Gentile " + name + ", la sua prenotazione per il giorno " + prenotation  + " per "+ str(seatsNumber) +" è stata effettuata con successo."
  #invia notifica tramite faas
  payload = {'to': to, 'soggetto': soggetto, 'messaggio': messaggio}
  r = requests.get('http://gateway.openfaas:8080/function/notifation-faas-service',data=json.dumps(payload))
  if currentSeats + seatsNumber <= totalSeats: 
   return Response(status=200, mimetype='application/json')
  else:
   return Response(status=403, mimetype='application/json')

#Endpoint per liveness
@app.route("/healtz")
def healtz():
 return Response(status=200, mimetype='application/json') 

if __name__ == "__main__":
    app.run(debug=True,port=8081,host='0.0.0.0')
